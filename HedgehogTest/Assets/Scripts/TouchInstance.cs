﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchInstance : MonoBehaviour {

    public GameObject PrefabToInstance;

    void OnMouseDown()
    {
            Vector3 _euler = transform.eulerAngles;
            _euler.z = Random.Range(0, 360);
            Instantiate(PrefabToInstance, new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y,0.0f), Quaternion.Euler(_euler));
    }
}
