﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HedgehogTouch : MonoBehaviour {

    [Range (0.0f, 500.0f)]
    public float MaxTouchForce = 10.0f;

    [Range(0.0f, 500.0f)]
    public float MaxTouchTorque = 10.0f;

    Rigidbody2D turnBody;
    float currentAngle;
    public AnimationCurve ac;

    private void Start()
    {

        turnBody = GetComponent<Rigidbody2D>();
    }

    public  void OnMouseDown()
    {
        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(0.0f, MaxTouchForce), Random.Range(0.0f, MaxTouchForce)));
        gameObject.GetComponent<Rigidbody2D>().AddTorque(Random.Range(-MaxTouchTorque, MaxTouchTorque));
        StartCoroutine(SizeAnimation());
    }

    IEnumerator SizeAnimation()
    {
        float timer = 0.0f;
        while (timer <=1.0f)
        {
            float sizeMult = Mathf.Lerp(0f, 1f, ac.Evaluate(timer / 1.0f));
            transform.localScale = new Vector2(sizeMult,sizeMult) ;
            timer += Time.deltaTime;
            yield return null;
        }
    }
}
