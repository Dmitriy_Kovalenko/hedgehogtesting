﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorLerper : MonoBehaviour {

    public float speed;
    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Hedgehog")
        {
            other.GetComponent<Rigidbody2D>().MoveRotation(Mathf.LerpAngle(other.GetComponent<Rigidbody2D>().rotation, 0, speed * Time.deltaTime));
        }
    }
}
